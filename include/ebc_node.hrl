-record(tx_in, {
			previous_output = undefined,
			previous_index = undefined,
			script_length = undefined,
			signature_script = undefined,
			sequence = undefined
		}).

-record(tx_out, {
			value = undefined,
			pk_script_length = undefined,
			pk_script = undefined
		}).

-record(tx, {
			hash = undefined,
			version = undefined,
			tx_in_count = undefined,
			tx_in = undefined,
			tx_out_count = undefined,
			tx_out = undefined,
			lock_time = undefined
	   }).

