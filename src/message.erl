-module(message).

-export([signMessage/2, verifyMessage/3]).

-compile([export_all, debug_info]).

-define(CURVE, secp256k1).

formatMessage(Message) when is_binary(Message) ->
	cryptopp:sha256(cryptopp:sha256(<<"Bitcoin Signed Message:\n", Message/binary>>)).

verifyMessage(Message, Sig, PublicKey) ->
	ok.

signMessage(PrivateKey, Message) ->
	%returns a concatination of R and S.
	Sig = cryptopp:ecdsa_sign(?CURVE, PrivateKey, formatMessage(Message)),
	base64:encode(Sig).
