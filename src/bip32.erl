-module(bip32).
-export([]).

-compile([export_all, debug_info]).

-define(CURVE, secp256k1).

-define(MAIN_PUBLIC, 16#0488B21E).
-define(MAIN_PRIVATE, 16#0488ADE4).
-define(TEST_PUBLIC, 16#043587CF).
-define(TEST_PRIVATE, 16#04358394).
-define(LITE_PUBLIC, 16#019da462).
-define(LITE_PRIVATE, 16#019d9cfe).

-define(MAINNET_VERSION, 16#00).
-define(TESTNET_VERSION, 16#6F).
-define(LITENET_VERSION, 16#30).
-define(LITENET_TEST_VERSION, 16#6F).

-record(walletMeta, {
	numberAccounts,
	master,
	version
}).

-record(chainMeta, {
	id,
	numberAddresses
}).

-record(addressMeta, {
	id,
	address,
	label = "",
	visible = true
}).

-record(accountMeta, {
	id,
	label = "",
	visible = true,
	internal,
	external
}).

% Some functions to help in generating wallets and addresses
mainnet_identifiers() ->
	{?MAIN_PUBLIC, ?MAIN_PRIVATE}.

testnet_identifiers() ->
	{?TEST_PUBLIC, ?TEST_PRIVATE}.

getCorrespondingIdent(?MAIN_PUBLIC) -> ?MAIN_PRIVATE;
getCorrespondingIdent(?MAIN_PRIVATE) -> ?MAIN_PUBLIC;
getCorrespondingIdent(?TEST_PUBLIC) -> ?TEST_PRIVATE;
getCorrespondingIdent(?TEST_PRIVATE) -> ?TEST_PUBLIC.

getPublicIdent(?MAIN_PRIVATE) -> ?MAIN_PUBLIC;
getPublicIdent(?MAIN_PUBLIC) -> ?MAIN_PUBLIC;
getPublicIdent(?TEST_PRIVATE) -> ?TEST_PUBLIC;
getPublicIdent(?TEST_PUBLIC) -> ?TEST_PUBLIC.

getPrivateIdent(?MAIN_PRIVATE) -> ?MAIN_PRIVATE;
getPrivateIdent(?MAIN_PUBLIC) -> ?MAIN_PRIVATE;
getPrivateIdent(?TEST_PRIVATE) -> ?TEST_PRIVATE;
getPrivateIdent(?TEST_PUBLIC) -> ?TEST_PRIVATE.

mainnet_version() -> ?MAINNET_VERSION.
testnet_version() -> ?TESTNET_VERSION.

getAddressVersion(?MAIN_PUBLIC) -> mainnet_version();
getAddressVersion(?MAIN_PRIVATE) -> mainnet_version();
getAddressVersion(?TEST_PUBLIC) -> testnet_version();
getAddressVersion(?TEST_PRIVATE) -> testnet_version();
getAddressVersion(_) -> error.

%% @doc Generate the chain code that will be used for a wallet. It will be
%% 256 bits long (32 bytes). I will use crypto:rand_bytes/1 for now, but
%% will probably replace it with a cryptopp function to remove dependence on
%% crypto
%%
getChainCode() ->
	crypto:rand_bytes(32).

%% @doc Generate an ECC private key, this will be the private key used by
%% the derivation function to drive the wallet addresses etc. Technically
%% any number less than the modulus of the curve will do, but rather use
%% the key generated buy cryptopp to make sure that it is strong.
getPrivateKey() -> 
	cryptopp:ecdsa_generate_private_key(?CURVE).

getPublicKey({private, PrivateKey}) ->
	getPublicKey(PrivateKey);
getPublicKey(PrivateKey) ->
	cryptopp:ecdsa_compress_point(cryptopp:ecdsa_generate_public_key(?CURVE, PrivateKey)).

neuterKey({private, _PrivateKey} = Kpriv) ->
	{public, getPublicKey(Kpriv)}.

%% @doc Generate the primary extended private key, this will consist of
%% (k, c) when k is the private key itself, and c is the chain code
getExtendedPrivateKey() ->
	{{private, getPrivateKey()}, getChainCode()}.

%% @doc Calculate the private component for Private Child Key Derivation
privCKD_1({K, C}, I) ->
	I_bin = <<I:32/big>>,
	cryptopp:hmac_sha512(C, <<0, K/binary, I_bin/binary>>).

%% @doc Calculate the public component for Private Child Key Derivation
privCKD_0({K, C}, I) ->
	I_bin = <<I:32/big>>,
	K_pub = getPublicKey(K),
	cryptopp:hmac_sha512(C, <<K_pub/binary, I_bin/binary>>).

%% @doc Calculate the private key derivation for a passed extended private key
%% for a particular iteration - the derivation is done in two forms depdending if
%% it is public or private
privCKD({{private, K}, C}, I) ->
	It = case ((I band 16#80000000) =:= 16#80000000) of
	          true -> privCKD_1({K, C}, I);
		  false -> privCKD_0({K, C}, I)
	      end,
	<<Il:32/binary, Ir:32/binary>> = It,
	Ki = (binary:decode_unsigned(Il) + binary:decode_unsigned(K)) rem cryptopp:ecdsa_get_modulus(?CURVE),
	Ci = Ir,
	{{private, binary:encode_unsigned(Ki)}, Ci}.

%% @doc Calculate the public component for Private Child Key Derivation
pubCKD_0({K, C}, I) ->
	I_bin = <<I:32/big>>,
	K_pub = K,
	cryptopp:hmac_sha512(C, <<K_pub/binary, I_bin/binary>>).

%% K is a {x,y} point (public key)
pubCKD({{public, K}, C}, I) ->
	It = case (I band 16#80000000) =:= 16#80000000 of
	          true -> error;
		  false -> pubCKD_0({K, C}, I)
	      end,
	<<Il:32/binary, Ir:32/binary>> = It,
	G = cryptopp:ecdsa_get_base_point(?CURVE),	%get the base point
	Il_point = cryptopp:ecdsa_point_multiplication(?CURVE, Il, G),
	Ki = cryptopp:ecdsa_compress_point(cryptopp:ecdsa_point_addition(?CURVE, Il_point, cryptopp:ecdsa_decode_point(?CURVE,K))),
	Ci = Ir,
	{{public, Ki}, Ci}.

%% get the identifier for an extended private key - but a public key could also be pass
getIdentifier({{private, K}, C}) ->
	Kpub = getPublicKey(K),
	getIdentifier({{public, Kpub}, C});

getIdentifier({{public, Kpub}, _C}) ->
	Ident = cryptopp:ripemd160(cryptopp:sha256(Kpub)),
	<<Fingerprint:32, _Rest/binary>> = Ident,
	{<<Fingerprint:32/big>>, Ident}.
	
%% return a fingerprint for the master key's parent
getFingerprint({<<0:128>>,<<0:128>>}) ->
	<<0:32>>;
getFingerprint({K,C}) ->
	{Fingerprint, _Ident} = getIdentifier({K, C}),
	Fingerprint.

%% generate the master key from a seed
generateMasterKey(Seed)	->
	I = cryptopp:hmac_sha512(<<"Bitcoin seed">>, Seed),
	<<Il:32/binary, Ir:32/binary>> = I,
	{{private, Il}, Ir}.

generateMasterKey() ->
	Seed = crypto:rand_bytes(32),
	generateMasterKey(Seed).

priv(I) ->
	I bor 16#80000000.

%% CHAIN GENERATION FUNCTIONS %%
generatePrivChain(M, []) -> 
	{<<0:32>>, 0, 0, M};
generatePrivChain(M, [P]) ->
	{getFingerprint(M), 1, P, privCKD(M, P)};
generatePrivChain(M, [P | Ps] = L) ->
	{_ParentFPR, _PDepth, _PChild, ExtCode} = generatePrivChain(M, Ps), 
	MyExtCode = privCKD(ExtCode, P),
	ParentFinger = getFingerprint(ExtCode),
	{ParentFinger, length(L), P, MyExtCode}.

generatePrivChainFromRoot(ExtendedKey, Depth, Parent, Child, []) ->
	{Parent, Depth, Child, ExtendedKey};
generatePrivChainFromRoot(ExtendedKey, Depth, _Parent, _Child, [C]) ->
	{getFingerprint(ExtendedKey), Depth+1, C, privCKD(ExtendedKey, C)};
generatePrivChainFromRoot(ExtendedKey, Depth, Parent, Child, [C|Cs] = L) ->
	{_PFinger, _PDepth, _PChild, PExtendedKey} = generatePrivChainFromRoot(ExtendedKey, Depth, Parent, Child, Cs),
	MyExtendedKey = privCKD(PExtendedKey, C),
	{getFingerprint(PExtendedKey), Depth+length(L), C, MyExtendedKey}.

generatePubChain(M, []) ->
	{<<0:32>>, 0, 0, M};
generatePubChain(M, [P]) ->
	{getFingerprint(M), 1, P, pubCKD(M, P)};
generatePubChain(M, [P | Ps] = L) ->
	{_ParentFPR, _PDepth, _PChild, ExtCode} = generatePubChain(M, Ps), 
	MyExtCode = pubCKD(ExtCode, P),
	ParentFinger = getFingerprint(ExtCode),
	{ParentFinger, length(L), P, MyExtCode}.

generatePubChainFromRoot(ExtendedKey, Depth, Parent, Child, []) ->
	{Parent, Depth, Child, ExtendedKey};
generatePubChainFromRoot(ExtendedKey, Depth, _Parent, _Child, [C]) ->
	{getFingerprint(ExtendedKey), Depth+1, C, pubCKD(ExtendedKey, C)};
generatePubChainFromRoot(ExtendedKey, Depth, Parent, Child, [C|Cs] = L) ->
	{_PFinger, _PDepth, _PChild, PExtendedKey} = generatePubChainFromRoot(ExtendedKey, Depth, Parent, Child, Cs),
	MyExtendedKey = pubCKD(PExtendedKey, C),
	{getFingerprint(PExtendedKey), Depth+length(L), C, MyExtendedKey}.

generateChainFromRoot(?MAIN_PRIVATE, ExtendedKey, Depth, Parent, Child, ChildSpec) ->
	generatePrivChainFromRoot(ExtendedKey, Depth, Parent, Child, ChildSpec);
generateChainFromRoot(?TEST_PRIVATE, ExtendedKey, Depth, Parent, Child, ChildSpec) ->
	generatePrivChainFromRoot(ExtendedKey, Depth, Parent, Child, ChildSpec);
generateChainFromRoot(?MAIN_PUBLIC, ExtendedKey, Depth, Parent, Child, ChildSpec) ->
	generatePubChainFromRoot(ExtendedKey, Depth, Parent, Child, ChildSpec);
generateChainFromRoot(?TEST_PUBLIC, ExtendedKey, Depth, Parent, Child, ChildSpec) ->
	generatePubChainFromRoot(ExtendedKey, Depth, Parent, Child, ChildSpec);
generateChainFromRoot(_, _ExtendedKey, _Depth, _Parent, _Child, _ChildSpec) ->
	{error, unknown_version}.

%% serialize the chain in the format specified by BIP32, the serialisation will return
%% both the seralised public and private entities - K is the private key
serializePrivChain({K,C}, Version, Depth, Parent, Child) ->
	<<Version:32/big, Depth:8, Parent/binary, Child:32/big, C/binary, 0, K/binary>>.

serializePubChain({Kpub,C}, Version, Depth, Parent, Child) ->
	<<Version:32/big, Depth:8, Parent/binary, Child:32/big, C/binary, Kpub/binary>>.

deserializePrivChain(K) ->
	<<Version:32/big, Depth:8, Parent:4/binary, Child:32/big, C:32/binary, 0, Key/binary>> = K,
	{{{private, Key}, C}, Version, Depth, Parent, Child}.

deserializePubChain(K) ->
	<<Version:32/big, Depth:8, Parent:4/binary, Child:32/big, C:32/binary, Key/binary>> = K,
	{{{public, Key}, C}, Version, Depth, Parent, Child}.

deserializeChain(<<?MAIN_PRIVATE:32/big, _Rest/binary>> = K) -> deserializePrivChain(K);
deserializeChain(<<?TEST_PRIVATE:32/big, _Rest/binary>> = K) -> deserializePrivChain(K);
deserializeChain(<<?MAIN_PUBLIC:32/big, _Rest/binary>> = K) -> deserializePubChain(K);
deserializeChain(<<?TEST_PUBLIC:32/big, _Rest/binary>> = K) -> deserializePubChain(K);
deserializeChain(_K) -> {error, unknown_version}.

serializeChain({{private, K}, C}, Version, Depth, Parent, Child) ->
	serializePrivChain({K, C}, Version, Depth, Parent, Child);
serializeChain({{public, K},C}, Version, Depth, Parent, Child) ->
	serializePubChain({K, C}, Version, Depth, Parent, Child).

%% Perform a hash160 on some passed data
hash160(Data) ->
	cryptopp:ripemd160(cryptopp:sha256(Data)).

%% this will convert the incoming data into the equivillant Base58 format
%% this representation include a 4-byte checksum generated from the first
%% four bytes of the double-sha256 hash of the data that is appended to the stream.
convertToBase58(Data) ->
	<<Checksum:32, _Rest/binary>> = cryptopp:sha256(cryptopp:sha256(Data)),
	CheckedData = <<Data/binary, Checksum:32/big>>,
	base58:binary_to_base58(CheckedData).

convertFromBase58(Data) ->
	BinaryData = base58:base58_to_binary(Data),
	DataSize = byte_size(BinaryData)-4,
	<<Payload:DataSize/binary, Checksum:32/big>> = BinaryData,

	%calculate the checksum and compare
	<<CalcChecksum:32, _Rest/binary>> = cryptopp:sha256(cryptopp:sha256(Payload)),
	case Checksum =:= CalcChecksum of
		true -> {ok, Payload};
		false -> {error, Payload}
	end.

%% Get a bitcoin address from the passed public key
getBitcoinAddress(Version, {private, K}) ->
	getBitcoinAddressFromPrivateKey(Version, K);
getBitcoinAddress(Version, {public, K}) ->
	getBitcoinAddressFromPublicKey(Version, K).

getBitcoinAddressFromPrivateKey(Version, K) ->
	Kpub1 = hash160(getPublicKey(K)),
	Kpub2 = <<Version, Kpub1/binary>>,
	convertToBase58(Kpub2).

getBitcoinAddressFromPublicKey(Version, Kpub) ->
	Kpub1 = hash160(Kpub),
	Kpub2 = <<Version, Kpub1/binary>>,
	convertToBase58(Kpub2).

getBitcoinAddressFromHash(Version, Hash) when is_integer(Hash) ->
	getBitcoinAddressFromHash(Version, <<Hash:160>>);

getBitcoinAddressFromHash(Version, Hash) when is_binary(Hash) ->
	K = <<Version, Hash/binary>>,
	convertToBase58(K).

%% Get a compressed public key from a passed private key
getCompressedPublicKey({{private, _K} = Kpriv, ChainCode}) ->
	{getCompressedPublicKey(Kpriv), ChainCode};
getCompressedPublicKey({private, _K} = Kpriv) ->
	{public, getPublicKey(Kpriv)};
getCompressedPublicKey({{public, _K} = Kpub, ChainCode}) ->
	{getCompressedPublicKey(Kpub), ChainCode};
getCompressedPublicKey({public, Kpub}) ->
	{public, cryptopp:ecdsa_compress_point(cryptopp:ecdsa_decode_point(secp256k1, Kpub))}.

getUncompressedPublicKey({{private, _K} = Kpriv, ChainCode}) ->
	{getUncompressedPublicKey(Kpriv), ChainCode};
getUncompressedPublicKey({private, _K} = Kpriv) ->
	getUncompressedPublicKey(getCompressedPublicKey(Kpriv));
getUncompressedPublicKey({{public, _K} = Kpub, ChainCode}) ->
	{getUncompressedPublicKey(Kpub), ChainCode};
getUncompressedPublicKey({public, Kpub}) ->
	{public, cryptopp:ecdsa_encode_public_key(cryptopp:ecdsa_decode_point(secp256k1, Kpub))}.

%% Get the Wallet Import Format (WIF) for a specified Private Key
getWalletImportFormat({private, K}) ->
	Kaug = <<16#80, K/binary>>,
	convertToBase58(Kaug).

%get the private key in hexidecimal representation
getHexPrivateKey({private, K}) ->
	{ok, hex(K)};
getHexPrivateKey(_) ->
	{error, no_private_key}.

%% Dump the details of an chain to the screen
dumpChain({K,C}, Parent, Depth, Child) ->
	{Fingerprint, Ident} = getIdentifier({K,C}),
	MainAddr = getBitcoinAddress(0, getCompressedPublicKey(K)),
	UncompressedMainAddr = getBitcoinAddress(0, getUncompressedPublicKey(K)),
	Kpub = getCompressedPublicKey(K),
	{private, Kpriv} = K,
	
	Pub = serializeChain({Kpub,C}, ?MAIN_PUBLIC, Depth, Parent, Child),
	Priv = serializeChain({K,C}, ?MAIN_PRIVATE, Depth, Parent, Child), 

	io:format("  * Identifier~n"),
	io:format("    * (hex):\t\t~p~n    * (fpr):\t\t~p~n    * (com addr):\t~p~n    * (uncom addr):\t~p~n", [hex(Ident), hex(Fingerprint), MainAddr, UncompressedMainAddr]),
	io:format("  * Secret Key~n"),
	io:format("    * (hex):\t\t~p~n    * (wip):\t\t~p~n", [hex(Kpriv), getWalletImportFormat(K)]),
	io:format("  * Public Key~n"),
	io:format("    * (hex):\t\t~p~n    * (hex):\t\t~p~n", [hex(Kpub), hex(getUncompressedPublicKey(Kpub))]),
	io:format("  * Chain Code~n"),
	io:format("    * (hex):\t\t~p~n", [hex(C)]),
	io:format("  * Serialized~n"),
	io:format("    * (pub hex):\t~p~n    * (prv hex):\t~p~n    * (pub b58):\t~p~n    * (prv b58):\t~p~n", [hex(Pub), hex(Priv), convertToBase58(Pub), convertToBase58(Priv)]).

dumpPubChain({Kpub,C}, Parent, Depth, Child) ->
	{Fingerprint, Ident} = getIdentifier({Kpub, C}),
	MainAddr = getBitcoinAddress(0, getCompressedPublicKey(Kpub)),
	UncompressedMainAddr = getBitcoinAddress(0, getUncompressedPublicKey(Kpub)),
	Pub = serializeChain({Kpub,C}, ?MAIN_PUBLIC, Depth, Parent, Child),
	{public, PubKey} = Kpub,

	io:format("  * Identifier~n"),
	io:format("    * (hex):\t\t~p~n    * (fpr):\t\t~p~n    * (com addr):\t~p~n    * (uncom addr):\t~p~n", [hex(Ident), hex(Fingerprint), MainAddr, UncompressedMainAddr]),
	io:format("  * Public Key~n"),
	io:format("    * (hex):\t\t~p~n    * (hex):\t\t~p~n", [hex(PubKey), hex(getUncompressedPublicKey(Kpub))]),
	io:format("  * Chain Code~n"),
	io:format("    * (hex):\t\t~p~n", [hex(C)]),
	io:format("  * Serialized~n"),
	io:format("    * (pub hex):\t~p~n    * (pub b58):\t~p~n", [hex(Pub), convertToBase58(Pub)]).

	
%%Test vectors for the private key chain
testDump() ->
	B = <<0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15>>,
	{Parent, Depth, Child, M} = generatePrivChain(generateMasterKey(B), []),
	dumpChain(M, Parent, Depth, Child).

testDump(P) ->
	B = <<0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15>>,
	{Parent, Depth, Child, M} = generatePrivChain(generateMasterKey(B), P),
	dumpChain(M, Parent, Depth, Child).

testDumpFromSerial(SerialisedPrivKey) ->
	{ok, Payload} = convertFromBase58(SerialisedPrivKey),
	{M, _Version, Depth, Parent, Child} = deserializePrivChain(Payload),
	dumpChain(M, Parent, Depth, Child).	

%%Test vectors for the public key chain, the public key will derive from the private key
testPubDump() ->
	B = <<0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15>>,
	{B_master, B_chain} = generateMasterKey(B),
	B_pub = {public, getPublicKey(B_master)},
	{Parent, Depth, Child, M} = generatePubChain({B_pub, B_chain}, []),
	dumpPubChain(M, Parent, Depth, Child).

testPubDump(P) ->
	B = <<0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15>>,
	{B_master, B_chain} = generateMasterKey(B),
	B_pub = {public, getPublicKey(B_master)},
	{Parent, Depth, Child, M} = generatePubChain({B_pub, B_chain}, P),
	dumpPubChain(M, Parent, Depth, Child).

testPubDumpFromSerial(SerialisedPubKey) ->
	{ok, Payload} = convertFromBase58(SerialisedPubKey),
	{M, _Version, Depth, Parent, Child} = deserializePubChain(Payload),
	dumpPubChain(M, Parent, Depth, Child).

testPubRangeGeneration(_SerialisedPubKey, _Start, 0) ->
	ok;
testPubRangeGeneration(SerialisedPubKey, Start, Count) ->
	{ok, Payload} = getPublicRoot(SerialisedPubKey, [Start]),
	{ok, MainAddr, UncompressedAddr} = getPublicAddress(Payload),
	
	io:format("* Identifier~n"),
	io:format("    * (hex):\t\t~p~n    * (child):\t\t~p~n", [Payload, Start]),
	io:format("    * (com addr):\t~p~n    * (uncom addr):\t~p~n", [MainAddr, UncompressedAddr]),

	testPubRangeGeneration(SerialisedPubKey, Start+1, Count-1).

%%%%%%%% Wallet Management Functions %%%%%%%%%

getPrivateMaster(MasterKey) when is_binary(MasterKey) ->
	{_PublicIdentifier, PrivateIdentifier} = mainnet_identifiers(),
	getPrivateMaster(MasterKey, PrivateIdentifier).
 
getPrivateMaster(MasterKey, Version) when is_binary(MasterKey) ->
	{Parent, Depth, Child, ExtendedKey} = generatePrivChain(generateMasterKey(MasterKey), []),
	{ok, convertToBase58(serializeChain(ExtendedKey, Version, Depth, Parent, Child)),
	     convertToBase58(serializeChain(getCompressedPublicKey(ExtendedKey), getPublicIdent(Version), Depth, Parent, Child))}.

%% Get the public address from a serialised public key, Serialised Root can be
%% any serialised key, and Child can either by a normal hardened key.
%% Childspec is a list of how the child should be derived - The childspec is reversed from expected
getPublicRoot(SerialisedRoot, ChildSpec) ->
	case convertFromBase58(SerialisedRoot) of
		{ok, KeyPayload} -> 
			{ExtendedKey, Version, Depth, Parent, Child} = deserializeChain(KeyPayload),
			{MyParent, MyDepth, MyChild, MyExtendedKey} = generateChainFromRoot(getPublicIdent(Version), getCompressedPublicKey(ExtendedKey), Depth, Parent, Child, ChildSpec),
			{ok, convertToBase58(serializeChain(MyExtendedKey, getPublicIdent(Version), MyDepth, MyParent, MyChild))};
		{error, _KeyPayload} ->
			{error, public_key_checksum}
	end.

getPrivateRoot(SerialisedRoot, ChildSpec) ->
	case convertFromBase58(SerialisedRoot) of
		{ok, KeyPayload} -> 
			{ExtendedKey, Version, Depth, Parent, Child} = deserializeChain(KeyPayload),
			{MyParent, MyDepth, MyChild, MyExtendedKey} = generateChainFromRoot(getPrivateIdent(Version), ExtendedKey, Depth, Parent, Child, ChildSpec),
			{ok, convertToBase58(serializeChain(MyExtendedKey, Version, MyDepth, MyParent, MyChild)),
			     convertToBase58(serializeChain(getCompressedPublicKey(MyExtendedKey), getPublicIdent(Version), MyDepth, MyParent, MyChild))};
		{error, _KeyPayload} ->
			{error, public_key_checksum}
	end.


%% Get the private key from a passed serialised root
getPrivateKey(SerialisedRoot) ->
	case convertFromBase58(SerialisedRoot) of
		{ok, KeyPayload} -> 
			{{Key, _ChainCode}, _Version, _Depth, _Parent, _Child} = deserializeChain(KeyPayload),
			getHexPrivateKey(Key);
		{error, _KeyPayload} ->
			{error, public_key_checksum}
	end.

%% Get the public key from a passed serialised root
getPublicKeys(SerialisedRoot) ->
	case convertFromBase58(SerialisedRoot) of
		{ok, KeyPayload} -> 
			{{Key, _ChainCode}, _Version, _Depth, _Parent, _Child} = deserializeChain(KeyPayload),
			{public, CompressedKey} = getCompressedPublicKey(Key),
			{public, UncompressedKey} = getUncompressedPublicKey(Key),
			{ok, hex(CompressedKey), hex(UncompressedKey)};
		{error, _KeyPayload} ->
			{error, public_key_checksum}
	end.

%% Get the wallet addresses for a passed serialised root,
%% on success {ok, CompressedAddress, UncompressedAddress} is returned
getPublicAddress(SerialisedRoot) ->
	case convertFromBase58(SerialisedRoot) of
		{ok, KeyPayload} -> 
			{{Kpub, _ChainCode}, Version, _Depth, _Parent, _Child} = deserializeChain(KeyPayload),
			MainAddr = getBitcoinAddress(getAddressVersion(Version), getCompressedPublicKey(Kpub)),
			UncompressedMainAddr = getBitcoinAddress(getAddressVersion(Version), getUncompressedPublicKey(Kpub)),
			{ok, MainAddr, UncompressedMainAddr};
		{error, _KeyPayload} ->
			{error, public_key_checksum}
	end.

%%%%%%%% Wallet Management Functions %%%%%%%%%
openWallet(Ref, Filename) -> 
	dets:open_file(Ref, [{file, Filename}]).

closeWallet(Ref) ->
	dets:close(Ref).

walletGetMeta(Ref) ->
	case dets:lookup(Ref, walletMeta) of
		[Wallet] when is_record(Wallet, walletMeta) -> Wallet;
		[] -> {error, no_metadata}
	end.

walletGetMetaVersion(Ref) ->
	case walletGetMeta(Ref) of
		#walletMeta{version = Version} -> Version;
		Error -> Error
	end.

walletGetKey(_Ref, 0) ->
	{<<0:128>>, <<0:128>>};
walletGetKey(Ref, Identifier) ->
	case dets:lookup(Ref, Identifier) of
		[{Identifier, ExtKey, _Depth, _Parent, _ChildIndex}] ->
			ExtKey;
		[] -> {error, no_key_for_identifier}
	end.

walletGetChain(Ref, Identifier) ->
	case dets:lookup(Ref, Identifier) of
		[Ident] ->
			Ident;
		[] -> {error, no_key_for_identifier}
	end.


initialiseWallet(Ref, Seed, Version) ->
	dets:delete_all_objects(test),
	{Key, Chain} = generateMasterKey(Seed),
	{_Fingerprint, Identifier} = getIdentifier({{private, Key}, Chain}),
	Ident = hex(Identifier),
	
	updateWalletRecord(Ref, walletCreateRecord(Ident, {Key, Chain}, 0, 0, 0)),
	updateWalletRecord(Ref, #walletMeta{
				numberAccounts = -1,
				master = Ident,
				version = Version
			}).

%All accounts appear on depth 1 with the master node being the parent.
walletAddAccount(Ref, Comment)  ->
	case walletGetMeta(Ref) of
		#walletMeta{numberAccounts = NumberAccounts, master = MasterIdent} = Wallet  ->
			MasterKey = walletGetKey(Ref, MasterIdent),
			%create a new account in the wallet
			{_ParentFinger, _Depth, ChildIndex, AccountKey} = generatePrivChain(MasterKey, [priv(NumberAccounts+1)]),
			{_Account1Finger, _Depth1, ExternalIndex, ExternalKey} = generatePrivChain(AccountKey, [priv(0)]),
			{_Account2Finger, _Depth2, InternalIndex, InternalKey} = generatePrivChain(AccountKey, [priv(1)]),

			
			{_Fingerprint, AccountIdentifier} = getIdentifier(AccountKey),
			{_IntFingerprint, ExternalIdentifier} = getIdentifier(ExternalKey),
			{_ExtFingerprint, Internaldentifier} = getIdentifier(InternalKey),
			Ident = hex(AccountIdentifier),
			ExtIdent = hex(ExternalIdentifier),
			IntIdent = hex(Internaldentifier),

			%%we need to create the Account, the Internal Chain, and the External Chain
			updateWalletRecord(Ref, walletCreateRecord(Ident, AccountKey, 1, MasterIdent, ChildIndex)),
			updateWalletRecord(Ref, walletCreateRecord(ExtIdent, ExternalKey, 2, Ident, ExternalIndex)),
			updateWalletRecord(Ref, walletCreateRecord(IntIdent, InternalKey, 2, Ident, InternalIndex)),
			
			%%update the metadata
			walletUpdateChainMeta(Ref, Ident, #accountMeta{
							id = AccountIdentifier,
							label = Comment,
							internal = ExtIdent,
							external = IntIdent
			}),
			walletUpdateChainMeta(Ref, IntIdent, #chainMeta{
							id = Internaldentifier,
							numberAddresses = 0 
			}),
			walletUpdateChainMeta(Ref, ExtIdent, #chainMeta{
							id = ExternalIdentifier,
							numberAddresses = 0
			}),

			%%add one internal and external address
			walletAddAddressToChain(Ref, IntIdent, ""),
			walletAddAddressToChain(Ref, ExtIdent, ""),

			%add a reference to the chains address list
			walletUpdateChainAccounts(Ref, [Ident | walletGetChainAccounts(Ref)]),

			updateWalletRecord(Ref, Wallet#walletMeta{numberAccounts = NumberAccounts+1}),

			{ok, Ident, IntIdent, ExtIdent};
		Error -> Error
	end.

% Add an address to a specified chain (this will be at depth 3)
walletAddAddressToChain(Ref, Ident, Comment) ->
	Version = walletGetMetaVersion(Ref),
	case walletGetChainMeta(Ref, Ident) of
		#chainMeta{numberAddresses = NumberAddresses} = Chain ->
			ChainKey = walletGetKey(Ref, Ident),
			%generate the new address paring
			{_ParentFinger, _Depth, ChildIndex, AddressKey} = generatePrivChain(ChainKey, [priv(NumberAddresses)]),

			{_Fingerprint, AddressIdentifier} = getIdentifier(AddressKey),
			AddressIdent = hex(AddressIdentifier),

			%update it in the wallet
			updateWalletRecord(Ref, walletCreateRecord(AddressIdent, AddressKey, 3, Ident, ChildIndex)),
			
			%update the metadata
			walletUpdateChainMeta(Ref, AddressIdent, #addressMeta{
								id = AddressIdent,
								label = Comment,
								address = getBitcoinAddress(0, AddressKey)
			}),

			%add a reference to the chains address list
			walletUpdateChainAddresses(Ref, Ident, [AddressIdent | walletGetChainAddresses(Ref, Ident)]),

			%update the chains metadata
			walletUpdateChainMeta(Ref, Ident, Chain#chainMeta{numberAddresses = NumberAddresses+1});
		Error -> Error
	end.

%Export the wallet using the serialised format
walletExport(Ref) ->
	walletExport(Ref, walletGetMeta(Ref)).

walletExport(Ref, #walletMeta{master = MasterIdent, numberAccounts = NumberAccounts}) ->
	{Identifier, Key, Depth, Parent, Child} = walletGetChain(Ref, MasterIdent),
	ParentFingerprint = getFingerprint(walletGetKey(Ref, Parent)),
	io:format("~p~n", [convertToBase58(serializePrivChain(Key, ?MAIN_PRIVATE, Depth, ParentFingerprint, Child))]).
	

%Get the metadata associated with an account
walletGetChainMeta(Ref, Identifier) ->
	case dets:lookup(Ref, "meta_" ++ Identifier) of
		[{_Identifier, Meta}] ->
			Meta;
		[] ->
			{error, no_key_for_identifier}
	end.

walletGetChainAddresses(Ref, Identifier) ->
	case dets:lookup(Ref, "addresses_" ++ Identifier) of
		[{_Identifier, Addresses}] ->
			Addresses;
		[] ->
			[]
	end.

walletGetChainAccounts(Ref) ->
	case dets:lookup(Ref, "accounts") of
		[{_Identifier, Accounts}] ->
			Accounts;
		[] ->
			[]
	end.

walletUpdateChainAddresses(Ref, Identifier, Addresses) ->
	updateWalletRecord(Ref, {"addresses_" ++ Identifier, Addresses}).

walletUpdateChainAccounts(Ref, Accounts) ->
	updateWalletRecord(Ref, {"accounts", Accounts}).

walletUpdateChainMeta(Ref, Identifier, Meta) ->
	updateWalletRecord(Ref, {"meta_" ++ Identifier, Meta}).

updateWalletMeta(Ref, #walletMeta{numberAccounts = NumberAccounts}) ->
	updateWalletRecord(Ref, {walletMeta, NumberAccounts}).

walletCreateRecord(Identifier, ExtKey, Depth, Parent, ChildIndex) ->
	{Identifier, ExtKey, Depth, Parent, ChildIndex}.

updateWalletRecord(Ref, Record) ->
	dets:insert(Ref, Record).

%%%%%% Ancillary Functions %%%%%%
hex({public, Kpub}) -> hex(Kpub);
hex({private, Kpriv}) -> hex(Kpriv);
hex(Binary) when is_binary(Binary) ->
	A = binary_to_list(Binary),
	lists:flatten(lists:map(fun hex_char/1, A)).

hex_char(X) ->
	lists:flatten(io_lib:format("~2.16.0b", [X])).
